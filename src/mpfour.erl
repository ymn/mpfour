#!/usr/bin/env escript

-module(mpfour).

-compile(export_all).

 -define(ATOM_SIZE, 8).

-record(atom, { type, size_base, start}).
-record(moov_atom, { mvhd, traks}).
-record(trak_atom, { tkhd, mdia, chunks_size, chunks, samples_size, samples}).

read(File) ->
  read(File, 0, 0, []).

read(File, Atom) ->
  read(File, Atom#atom.start + ?ATOM_SIZE,
    Atom#atom.start + Atom#atom.size_base - ?ATOM_SIZE).

read(File, Pos, End) ->
  read(File, Pos, End, []).

read(File, Pos, End, Atoms) when (End > Pos) or (End == 0) ->
  case file:pread(File, Pos, 8) of
    {ok, <<Size:32/integer, Type:4/binary>>} ->
      Atom = #atom{type=binary_to_list(Type), size_base=Size, start=Pos},
      read(File, Pos + Size, End, lists:append(Atoms, [Atom]));
    eof ->
      Atoms
  end;

read(_File, _Pos, _End, Atoms) -> Atoms.

getAtoms(Type, Atoms) ->
  lists:filter(fun(X) -> X#atom.type == Type end, Atoms).

getAtom(Type, Atoms) ->
  [Value | _] = getAtoms(Type, Atoms),
  Value.

parse_atom(File, Atom) ->
  parse_atom(File, Atom, Atom#atom.type).

parse_atom(File, Atom, "moov") ->
  Children = read(File, Atom),
  MvhdAtom = parse_atom(File, getAtom("mvhd", Children)),
  TrakAtoms = lists:map(fun(A) -> parse_atom(File, A) end, getAtoms("trak", Children)),
  #moov_atom{ mvhd=MvhdAtom, traks=TrakAtoms };

parse_atom(File, Atom, "mvhd") ->
  case file:pread(File, Atom#atom.start, Atom#atom.size_base) of
    {ok, <<_:20/binary, TimeScale:32, Duration:32,_:80/binary >>} ->
      {mvhd, {Duration / TimeScale}}
  end;

parse_atom(File, Atom, "trak") ->
  Children = read(File, Atom),
  TkhdAtom = parse_atom(File, getAtom("tkhd", Children)),
  #trak_atom{
    tkhd=TkhdAtom};

parse_atom(File, Atom, "tkhd") ->
  case file:pread(File, Atom#atom.start, Atom#atom.size_base) of
    {ok, <<_:84/binary, TrackWidth:32, TrackHeight:32>>} ->
      {tkhd, {TrackWidth, TrackHeight}}
  end;

parse_atom(File, Atom, "mdia") ->
  read(File, Atom).

can_open(File) ->
  lists:member(filename:extension(File), [".mp4"]).

main(Arg) ->
  AbsPath = filename:absname(Arg),
  case can_open(AbsPath) of
    true ->
      CurrentSize = filelib:file_size(AbsPath),
      {ok, File} = file:open(Arg, [read,binary,raw]),
      case parse_atom(File, getAtom("moov", read(File))) of
        {moov_atom,{mvhd,{Dur}},[{trak_atom,{tkhd,{A,B}},_,_,_,_,_},_]} ->
          BitRate = round((CurrentSize / Dur) * 8 / 1000),
          io:format("Bitrate: ~p kbps Width = ~p Height = ~p~n", [BitRate, round(A/65536), round(B/65536)]);
        C -> io:format("Get some strange staff ~p~n", [C]);
        _ -> io:format("error~n", [])
      end,
      file:close(File);
    false ->
      io:format("Illegal file~n", [])
  end.
